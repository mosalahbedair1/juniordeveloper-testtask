$(document).ready(function() {

  // Start Show/Hide the specific attributes
  $("#productType").change(function() {
    var type = $("#productType").val();
    $(".details").hide();
    $("." + type).show();
  });
  // End Show/Hide the specific attributes


  // Start Front-End Validation

  // Start string inputs validation
  $('#sku').blur(function () {
    // if you entered only spaces in the input field it will be a valid value, $.trim() method solves this problem
    if ($.trim($(this).val()) == '') {
      $(this).css('border', '1px solid #f00').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeIn(200)
    } else {
      $(this).css('border', '1px solid #080').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeOut(200)
    }
  });

  $('#name').blur(function () {
    if ($.trim($(this).val()) == '') {
      $(this).css('border', '1px solid #f00').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeIn(200)
    } else {
      $(this).css('border', '1px solid #080').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeOut(200)
    }
  });

  $('#productType').blur(function () {
    if ($(this).val() == 'Select Product Type') {
      $(this).css('border', '1px solid #f00').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeIn(200)
    } else {
      $(this).css('border', '1px solid #080').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeOut(200)
    }
  });
  // End string inputs validation


  // Start numeric-must inputs validation
  var inputs = ['#price', '#size', '#height', '#width', '#length', '#weight'];

  inputs.forEach(input => {
    $(`${input}`).blur(function () {
      if ($.trim($(this).val()) == '') {
        $(this).css('border', '1px solid #080').parent().find('.alert.alert-danger.fronEnd-alert.valid').fadeOut(200)
        $(this).css('border', '1px solid #f00').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeIn(200)
      } else if (Number.isNaN(Number($(this).val()))) {
        $(this).css('border', '1px solid #f00').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeOut(200)
        $(this).css('border', '1px solid #f00').parent().find('.alert.alert-danger.fronEnd-alert.valid').fadeIn(200)
      } else {
        $(this).css('border', '1px solid #f00').parent().find('.alert.alert-danger.fronEnd-alert.required').fadeOut(200)
        $(this).css('border', '1px solid #080').parent().find('.alert.alert-danger.fronEnd-alert.valid').fadeOut(200)
      }
    });
  });
  // End numeric-must inputs validation

  // End Fron-End Validation

});