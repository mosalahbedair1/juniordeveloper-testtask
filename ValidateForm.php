<?php
require_once('productHandler.php');
class ValidateForm
{
    private $data;
    private $errors=[];
    private $han;
    public function __construct($form_data)
    {
        $this->data = $form_data;
        $this->han= new Handler();
    }
    public function validateForm()
    {
        // checking if inputs are valid (back-end check)
        $this->validateSKU();
        $this->validateName();
        $this->validatePrice();
        $this->validateType();
        return $this->errors;
    }
    private function validateSKU()
    {
        $val = trim($this->data['SKU']);
        $found = $this->checkSKU($val);
        if (empty($val)) {
            $this->addError('SKU', 'SKU is required');
        } elseif ($found === true) {
            $this->addError('SKU', 'SKU should be uniqe');
        }
    }
    private function validateName()
    {
        $val = $this->data['name'];
        if (empty($val)) {
            $this->addError('name', 'Name is required');
        }
    }
    private function validatePrice()
    {
        $val = trim($this->data['price']);
        if (empty($val)) {
            $this->addError('price', 'Price is required');
        } elseif (!is_numeric($val)) {
            $this->addError('price', 'price must be integer');
        }
    }
    private function validateType()
    {
        $val = $this->data['type'];
        if ($val =='Select Product Type') {
            $this->addError('type', 'Type is required');
        }
    }
    private function addError($key, $error)
    {
        $this->errors[$key] = $error;
    }
    private function checkSKU($val)
    {
        $SKUs = $this->han->getAllSKU();
        $found = false;
        foreach ($SKUs as $SKU) {
            if ($SKU['SKU'] === $val) {
                $found = true;
                break;
            } else {
                $found = false;
            }
        }
        return $found;
    }
}
