<?php
include('Dvd.php');
include('Furniture.php');
include('Book.php');
require_once('ValidateForm.php');
if (isset($_POST['submit'])) {
    $validate = new ValidateForm($_POST);
    $errors = $validate->validateForm();

    // if there are errors --> don't do anything, No Errors? add product
    // NOTE: errors will be displayed in a span.error-text below at the alerts section in every input section
    if (array_filter($errors)) {
    } else {
        addProduct();
    }
}
  function addProduct()
  {
      $product =new $_POST['type']();
      $product->addProduct($_POST);
  }

?>
<!DOCTYPE html>
<html lang="en">
<?php include('templates/header.php') ?>


<div class="container">
  <form id="product_form"
    action="<?php echo $_SERVER['PHP_SELF']?>"
    method="POST">

    <!-- Start Header -->
    <nav>
      <h2 class='h1'>Product Add</h1>
        <div class='btns-container-sm'>
          <button type="submit" class="btn btn-success" name="submit" value="submit">Save</button>
          <button type="button" class="btn btn-danger" onClick="document.location.href='index.php'">Cancel</button>
        </div>
    </nav>
    <hr>
    <!-- End Header -->


    <!-- Start SKU input -->
    <div class='form-group row'>
      <label for="sku" class="form-label">SKU</label>
      <div class="col-sm-3">
        <input type="text" name="SKU" class="form-control" id="sku">
        <!-- Start Alerts -->
        <div class="alert alert-danger fronEnd-alert required" role="alert">
          Please, submit required data
        </div>
        <div class="alert alert-danger fronEnd-alert valid" role="alert">
          Please, submit a valid data
        </div>
      </div>
      <span class='error-text'><?php echo $errors['SKU'] ?? '' ?></span>
      <!-- End Alerts -->
    </div>
    <!-- End SKU input -->


    <!-- Start Name input -->
    <div class='form-group row'>
      <label for="name" class="form-label">Name</label>
      <div class="col-sm-3">
        <input type="text" name="name" class="form-control" id="name">
        <!-- Start Alerts -->
        <div class="alert alert-danger fronEnd-alert required" role="alert">
          Please, submit required data
        </div>
        <div class="alert alert-danger fronEnd-alert valid" role="alert">
          Please, submit a valid data
        </div>
      </div>
      <span class='error-text'><?php echo $errors['name'] ?? ''?></span>
      <!-- End Alerts -->
    </div>
    <!-- End Name input -->


    <!-- Start Price input -->
    <div class='form-group row'>
      <label for="price" class="form-label">Price (&dollar;)</label>
      <div class="col-sm-3">
        <input type="text" name="price" class="form-control" id="price">
        <!-- Start Alerts -->
        <div class="alert alert-danger fronEnd-alert required" role="alert">
          Please, submit required data
        </div>
        <div class="alert alert-danger fronEnd-alert valid" role="alert">
          Please, submit a valid data
        </div>
      </div>
      <span class='error-text'><?php echo $errors['price'] ?? ''?></span>
      <!-- End Alerts -->
    </div>
    <!-- End Price input -->


    <!-- Start Product Type input -->
    <div class='form-group row'>
      <label for="productType">Type Switcher</label>
      <div class="col-sm-3">
        <select class="form-select" id="productType" aria-label="Default select" name="type">
          <option selected>Select Product Type</option>
          <option value="DVD">DVD</option>
          <option value="Furniture">Furniture</option>
          <option value="Book">Book</option>
        </select>
        <!-- Start Alerts -->
        <div class="alert alert-danger fronEnd-alert required" role="alert">
          Please, submit required data
        </div>
        <div class="alert alert-danger fronEnd-alert valid" role="alert">
          Please, submit a valid data
        </div>
      </div>
      <span class='error-text'><?php echo $errors['type'] ?? ''?></span>
      <!-- End Alerts -->
    </div>
    <!-- End Product Type input -->


    <!-- Start Custom Properties inputs-->
    <div id="details-container">

      <!-- Start DVD Custom Property -->
      <div class="DVD details">
        <div class='form-group row'>
          <label for="size" class="form-label">Size (MB)</label>
          <div class="col-sm-3">
            <input type="text" name="size" class="form-control" id="size">
            <!-- Start Alerts -->
            <div class="alert alert-danger fronEnd-alert required" role="alert">
              Please, submit required data
            </div>
            <div class="alert alert-danger fronEnd-alert valid" role="alert">
              Please, submit a valid data
            </div>
            <div class="alert alert-info" role="alert">Please, provide the size in MB</div>
            <!-- End Alerts -->
          </div>
        </div>
      </div>
      <!-- End DVD Custom Property -->


      <!-- Start Furniture Custom Properties -->
      <div class="Furniture details">
        <!-- Start height input -->
        <div class='form-group row'>
          <label for="height" class="form-label">Height (CM)</label>
          <div class="col-sm-3">
            <input type="text" name="height" class="form-control" id="height">
            <!-- Start Alerts -->
            <div class="alert alert-danger fronEnd-alert required" role="alert">
              Please, submit required data
            </div>
            <div class="alert alert-danger fronEnd-alert valid" role="alert">
              Please, submit a valid data
            </div>
            <!-- End Alerts -->
          </div>
        </div>
        <!-- End height input -->
        <!-- Start width input -->
        <div class='form-group row'>
          <label for="width" class="form-label">Width (CM)</label>
          <div class="col-sm-3">
            <input type="text" name="width" class="form-control" id="width">
            <!-- Start Alerts -->
            <div class="alert alert-danger fronEnd-alert required" role="alert">
              Please, submit required data
            </div>
            <div class="alert alert-danger fronEnd-alert valid" role="alert">
              Please, submit a valid data
            </div>
            <!-- End Alerts -->
          </div>
        </div>
        <!-- End width input -->
        <!-- Start length input -->
        <div class='form-group row'>
          <label for="length" class="form-label">Length (CM)</label>
          <div class="col-sm-3">
            <input type="text" name="length" class="form-control" id="length">
            <!-- Start Alerts -->
            <div class="alert alert-danger fronEnd-alert required" role="alert">
              Please, submit required data
            </div>
            <div class="alert alert-danger fronEnd-alert valid" role="alert">
              Please, submit a valid data
            </div>
            <div class="alert alert-info" role="alert">Please, provide the dimensions in HxWxL</div>
            <!-- End Alerts -->
          </div>
        </div>
        <!-- End length input -->
      </div>
      <!-- End Furniture Custom Properties -->


      <!-- Start Book Custom Property -->
      <div class="Book details">
        <div class='form-group row'>
          <label for="weight" class="form-label">Weight (KG)</label>
          <div class="col-sm-3">
            <input type="text" name="weight" class="form-control" id="weight">
            <!-- Start Alerts -->
            <div class="alert alert-danger fronEnd-alert required" role="alert">
              Please, submit required data
            </div>
            <div class="alert alert-danger fronEnd-alert valid" role="alert">
              Please, submit a valid data
            </div>
            <div class="alert alert-info" role="alert">Please, provide the weight in KG</div>
            <!-- End Alerts -->
          </div>
        </div>
      </div>
      <!-- End Book Custom Property -->

    </div>
    <!-- End Custom Properties inputs -->

  </form>
</div>


<!-- Start Js -->
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
<!-- End Js -->


<?php include('templates/footer.php') ?>

</html>