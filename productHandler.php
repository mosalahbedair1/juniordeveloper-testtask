<?php
require_once('config/db_connection.php');
class Handler
{
    public function deleteRecords($data, $num)
    {
        $conn = db();
        for ($i = 0; $i < $num ; $i++) {
            $skuToDelete = $data['records'][$i];
            $sql = "DELETE FROM product where SKU = '$skuToDelete'";
            mysqli_query($conn, $sql);
        }
        header('Location:index.php');
    }

    // To view the products in the main page
    public function getProducts()
    {
        $conn = db();
        $sql = 'SELECT * FROM product';
        // make query and get result
        $result = mysqli_query($conn, $sql);
        // fetch the resulting rows as an array --> to make an iteration in the main page and show every product
        $products = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $products;
        mysqli_close($conn);
    }

    // To validate that the SKU in unique in the ValidateForm.php
    public function getAllSKU()
    {
        $conn = db();
        $sql = 'SELECT SKU FROM product';
        $result = mysqli_query($conn, $sql);
        $skus = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $skus;
        mysqli_close($conn);
    }
}
