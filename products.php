<?php
include_once('config/db_connection.php');

abstract class Product
{
    protected $SKU;
    protected $name;
    protected $price;
    protected $type;

    protected function setSKU($sku)
    {
        $this->SKU=$sku;
    }
    protected function setName($name)
    {
        $this->name=$name;
    }
    protected function setPrice($price)
    {
        $this->price=$price;
    }
    protected function setType($type)
    {
        $this->type=$type;
    }

    abstract protected function addProduct($product);
}
